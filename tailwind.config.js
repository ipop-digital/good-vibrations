// tailwind.config.js
// See more information about this file at https://tailwindcss.com/docs/installation#create-your-configuration-file

module.exports = {
  // prefix: 'tw-',
  important: true,
  mode: "jit",
  purge: {
    mode: "all",
    enabled: true,
    content: [
      "./src/**/*.js",
      "./templates/**/*.twig",
      "./templates/**/*.html",
      "./templates/*.html",
      "./templates/*.twig",
    ],
    options: {
      keyframes: true,
      fontFace: true,
    },
  },
  darkMode: "media", // See https://tailwindcss.com/docs/dark-mode
  theme: {
    fontSize: {},
    extend: {
      outline: {
        pink: ["1px solid #DB0366", "0px"],
      },
      flex: {
        2: 2,
        2.5: 2.5,
      },
      zIndex: {
        "-1": -1,
      },
      width: {},
      fontFamily: {},
      colors: {
        pink: "#DB0366",
        yellow: "#F1DC15",
        green: "#56AA6B",
        blue: "#4FBAD9",
        purple: "#9996C9",
      },
    },
  },
  variants: {},
  plugins: [],
};
